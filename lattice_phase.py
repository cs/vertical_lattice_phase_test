import picamera
import picamera.array
import time 
import numpy as np
import matplotlib.pyplot as plt
import pickle
import sys
print (sys.path)
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from numpy import exp, loadtxt, pi, sqrt
from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import MaxNLocator
import numpy as np
from matplotlib.colors import LinearSegmentedColormap
from PIL import Image
from matplotlib import pyplot
from numpy.fft import fft, ifft, fftfreq, fftshift
from numpy import fft
import time
from numpy import savetxt


#import natsort


"Importing data"
import os



left = 830
top = 430
right = 850
bottom = 520


plt.ion()
fig = plt.figure(figsize=(24,8))
ax = fig.add_subplot(111)
line1, = ax.plot([0,1], [0,1], 'b-')

camera = picamera.PiCamera()
phases = []
pixsums = []
for filename in camera.capture_continuous('img.jpg'):
    #print("Captured %s" % filename)
    data = filename
    im=[]
    crop2=[]
    #data = "img099.jpg"
    im = Image.open(data).convert("L")
    im1=im.crop((left,top,right,bottom))
    data2=np.asarray(im1)
    crop2.append(data2)
    
    allImages = np.array(crop2[0:])
    horizontalSum = allImages.sum(-1)
    
    x,y = np.indices(horizontalSum.shape)
    
    ft_allImages = fft.fftshift(fft.fft(horizontalSum, axis=-1), axes=-1)
    
    ii = np.argmax(np.abs(ft_allImages[:, 50:]), axis=-1)
    phase = np.angle(ft_allImages[:, ii+50])[:,0]
    phases.append(phase[0])
    pixsums.append(np.sum(im1))
    print(phase, phase*1.4*8.4, np.sum(im1))
    savetxt("phases.csv", phases)
    savetxt("pixsums.csv", pixsums)
    fig.clear()
    plt.subplot(121)
    plt.plot(np.arange(0, len(phases)), phases)
    #line1.set_xdata(np.arange(0, len(phases)))
    #line1.set_ydata(phases)
    plt.subplot(122)
    plt.imshow(im1)
    
    #plt.subplot(133)
    #plt.imshow(im)
    #plt.axvline(right)
    #plt.axvline(left)
    #plt.axhline(top)
    #plt.axhline(bottom)
    
    
    fig.canvas.draw()
    plt.show()
    time.sleep(2)
    
camera.close()
